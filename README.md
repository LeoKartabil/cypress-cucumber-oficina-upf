# Exemplo de Cypress com Cucumber para oficina na UPF
<br>
<img src="https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black" /> 
<img src="https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white" />
<img src="https://img.shields.io/badge/-cypress-%23E5E5E5?style=for-the-badge&logo=cypress&logoColor=058a5e" />

### Comandos para execução da automação

`npm install` - __Antes de rodar execute este comando para instalar as dependências__ <br>

`npm run cy:open` - __Executar em tela gráfica__ <br>

`npm run cy:run` - __Executar em  modo headless (somente no console)__ <br>

### Comandos para report

`npm run cy:clear` - __Limpa todos os reports (html e json) na pasta ./reports__

`npm run cy:report` - __Após a execução dos testes, gera o report HTML__

### Boas práticas na escrita Gherkin:
* 'Dado   // Given' - Pré-requsitos :: Dado que esteja.../ Dado que tenha.../ Dado que possua.../ Dado que inicie...
* 'Quando // When'  - Ações         :: Quando <verbo>.../ Quando preencher o formulário/ Quando realizar a requisição...
* 'Então  // Then'  - Validações    :: Então deverá <validação>.../ Então deverá ser visualizado o logo/

#### Evite utilizar primeira pessoa na escrita Gherkin.
* Ao invés de escrever: __Quando EU preencher o cadastro__
* Escreva: __Quando preencher o cadastro__
* `Lembre-se, quem está executando a ação é um robô!`
* Ou ainda, utilize atores: __Quando o administrador preencher o cadastro__

### O que continuar estudando?
* Testando seu app [https://docs.cypress.io/guides/end-to-end-testing/testing-your-app]
* Assertions (validações) [https://docs.cypress.io/guides/references/assertions#__docusaurus_skipToContent_fallback]
* Melhores práticas [https://docs.cypress.io/guides/references/best-practices]


#### Links
* Documentação do Cypress      [https://docs.cypress.io/guides]
* Github cucumber-preprocessor [https://github.com/TheBrainFamily/cypress-cucumber-preprocessor]
