/// <reference types="cypress" />

import {
  Given,
  When,
  Then,
  Before,
  After,
} from "cypress-cucumber-preprocessor/steps";

Before(() => {
  cy.log("Antes de tudo, isto é executado!");
  cy.clearLocalStorage();
  cy.clearCookies();
});

After(() => {
  cy.log("Depois de tudo, isto é executado!");
});

/** Pré-requisitos :: Dado :: Given */
Given(`que esteja na página de login`, () => {
  cy.visit("https://front.serverest.dev/login");
});

/** Ações :: Quando :: When */
When(`realizar login com {string} e {string}`, (email, senha) => {
  cy.log("Passo não implementado!");
  cy.log(">> E-mail: " + email);
  cy.log(">> Senha: " + senha);
});

When(`realizar login na aplicação com o usuário salvo`, () => {
  /** Para salvar o usuário você pode utilizar tanto
   * as variáveis de ambiente, quanto o comando cy.wrap()
   */
  cy.log("Passo não implementado!");
});

/** Validações :: Então :: Then */
Then(`deverá ser validado o login com sucesso`, () => {
  cy.log("Passo não implementado!");
});

Then(`deverá ser mostrado o alerta {string}`, (alert) => {
  cy.log("Passo não implementado!");
});
