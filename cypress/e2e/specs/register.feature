#language: pt

#Estes são exemplos complementares para que você
#continue exercitando a prática da automação com Cypress

Funcionalidade: Cadastro de usuários

#O contexto serve como passos que são executados antes de cada cenário
Contexto: Iniciar na pagína de login
    Dado que esteja na página de login

@register @regression
Cenario: Realizar cadastro com sucesso na tela de login
    Dado que esteja na tela de cadastro
    E que possua um usuário aleatório
    Quando cadastrar o usuário pelo front
#A informação entre aspas é encaminhada como parâmetro para o step
    Então deverá ser mostrado o alerta "Cadastro realizado com sucesso"
    E deverá ser validado o login com sucesso

@focus @register @regression
Cenario: Realizar cadastro com sucesso estando logado
    Dado que possua o primeiro usuário cadastrado na base
    E que possua um usuário aleatório
#Perceba que o passo abaixo, por se tratar de login, está no arquivo login.step.js
    Quando realizar login na aplicação com o usuário salvo
    E cadastrar um usuário estando logado
    Então deverá ser visualizado o usuário cadastrado na lista de usuários