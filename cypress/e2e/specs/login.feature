#language: pt

Funcionalidade: Login

@login @regression
Cenario: Realizar login no front.serverest com usuário cadastrado
    Dado que esteja na página de login
    Quando realizar login com "fulano@qa.com" e "teste"
    Então deverá ser validado o login com sucesso

@login @regression
Cenário: Tentar logar com usuário sem senha
    Dado que esteja na página de login
    Quando realizar login com "fulano@qa.com" e " "
    Então deverá ser mostrado o alerta "Senha é obrigatória"